console.log("Hello World")

function printName() {

	console.log("My name is John.");
};

printName();




function declaredFunction() {

	console.log("Hello World from declaredFunction()")
}

declaredFunction();



let variableFunction = function() {
	console.log("Hello Adgain");
};

variableFunction();

let funcExpression = function funcName() {

	console.log("Hello From the Other Side");
};




declaredFunction = function() {

	console.log("updated declaredFunction")
}

funcExpression = function() {

	console.log("updated funcExpression")
}

funcExpression();

const constantFunc = function(){

	console.log("Initialized with const!")

}




//Function Scoping

{

	let localVar = "Armando Perez"
	console.log(localVar)
}


let globalVar = "Mr. Worldwide";
console.log(globalVar);





function showNames(){

	var funtionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";	

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

};

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


	showNames();

	// nested Funtions


	function myNeewFunction(){

		let name = "Cee";

		function nestedFunction(){

			let nestedName = "Thor";
			console.log(name);
			console.log(nestedName);
		}
		nestedFunction();

	};
	myNewFunction();
	// nestedFunction();



	// Function and Global Scope Variables

		let globalName = "Nej";

		function newFunction2(){
			let nameInside = "Martin";
			console.log(globalName);
		};

		myNewFunction2();


//Using alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users
	//as opposed to a console.log() which will only show the message on the console
	//It allows us to show a short dialog or instruction to our user

	alert("Hello World");		

		function showSampleAlert(){
			alert("Hello User!")


		}

		showSampleAlert();

		