console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    

    function getPersonalDetails(){
	
		let fullName = prompt("Enter your Full Name:");
    	let age = prompt("Enter your age:");
    	let location = prompt("Enter your location:");


    	console.log("Hello, " + fullName);
    	console.log("You are " + age + " years old.");
    	console.log("You live in " + location);


    }

    getPersonalDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	

		let band1 = prompt("Enter first band's name:");
		let band2 = prompt("Enter second band's name:");
		let band3 = prompt("Enter third band's name:");
		let band4 = prompt("Enter fourth band's name:"); 
		let band5 = prompt("Enter fifth band's name:");
	

	function getBandList(){

		console.log("1." + band1)
		console.log("2." + band2)
		console.log("3." + band3)
		console.log("4." + band4)
		console.log("5." + band5)


	}	

	getBandList();	
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printMovieDetails(){
		let movie1 = "Rounders"
		let movie2 = "A New Hope"
		let movie3 = "A Most Violent Year"
		let movie4 = "Goodfellas"
		let movie5 = "The Godfather"

		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: 76%");
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: 69%");
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: 86%");
	}

	printMovieDetails();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);

